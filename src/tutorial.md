# Tutorial

> A story about someone called Donell writing down events of their life to evenor. Follow their steps to learn how to use evenor yourself.

Donell launches evenor and sees the `overview` screen. There is only one folder with data called `tutorial`. 

They pick a `Select folder?` dialogue and press `yes` to open the `profile` screen. Profile says that the name of the folder is `tutorial`, modification date is March 7th, and describes the structure of records. Each record is an `event` that has a `description`, a `date` and a media `file`. 

Donell locates the `View events?` dialogue and presses `yes` to see all the events they've written down so far. Recently, they've gone to the theatre and want to keep a memory of it.

They press the `plus` button at the top right to open a profile screen in the edit mode. At the top they can see the new identifier and below it all the various possible details about an event.

They locate the `Add description?` dialogue and press `yes`. An empty text input appears for them to type `went to the theatre`.

They locate the `Add date?` dialogue and press `yes` to add the date - `2004-04-04`.

Donell took a picture after the performance and wants to attach it to the record. They locate the `Add file?` dialogue and press `yes`. Evenor shows a file picker and they select the image from the gallery. Now, the profile shows several new sentences - the file name, extension and the hashsum. It's safe to edit the name and the extension, but the hashsum should stay the same, so that evenor can find the uploaded picture.

Donell presses `Save` and then `Back` to see the updated list of events. 

Now they notice that one of the events has an error - they actually visited Japan on December 12th, not January 1st! Donell locates the `Select event?` dialogue near the record and presses `yes`.

In the profile, Donell presses the `Edit` button at the top right and then finds the text input with the date. They fix the error and save the changes.

Now they want to see the memory about Everest, so they press `yes` near the `Select event?` dialogue. Oh no! They've uploaded the wrong image! They tap on the image for a long time and a context menu pops up, they press `Delete`. Donell decides to upload the correct image at another time.

> On desktop, right click instead of the long tap to open the context menu.

Back at the overview again, Donell decides to delete the note about cooking a lasagna - it doesn't feel right among stories of adventure. They tap on `cooked-lasagna` for a long time and a context menu pops up, `Delete`.

Donell decides to create a new folder for all the stories about cooking. They press `Back` to return to the list of folders, and press `plus` to create a new one. The profile shows all the various details about a folder. All the default values should suffice. They fill in the name input field and press `Save`.

To open the new folder, Donell locates the `View events?` dialogue and presses `yes.` The list of records is empty, so they press the `plus` button and fill in the memory about cooking a lasagna. They press `Save` and close the app.

> You have learned how to create, edit and delete records in an evenor folder.

To learn more about evenor, see the [User Guides](./user_guides.md).
