# Getting Started

Want to try evenor? Download the latest release from <https://norcivilianlabs.org>.

Already installed the app? Let's see how it works!

Open evenor for the first time to see the `overview` screen. Here is a list of `folders`, each folder full of data. Later you can add new folders using the `plus` button and search through them using the `search bar`. 

Now, find the `Select folder?` dialogue and press `yes` to see the `profile` screen. Profile tells you everything about a folder - its name, modification date, and the structure of the data inside of it. For example, each piece of data in the tutorial is an `event` that has a `description`, a `date` and a media `file`. You can learn more about the folder structure later on and even create your own to store any kind of data you can imagine! For now, locate the `View events?` dialogue and press `yes` to look inside the folder.

Do you see the overview screen again? Well done. This time the overview shows a list of `records`. This is a chain of events that happened to someone called Donell - the countries they visited, meals they ate and adventures that they've recorded using evenor. Feel free to play around in this folder, create new events and search through them with the search bar. When you feel ready, come back to the list of folders and press the plus button at the top right to start writing down your own story.

To learn more about evenor, see the [Tutorial](./tutorial.md) and the [User Guides](./user_guides.md).
