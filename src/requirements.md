# Requirements

## twenty vintage charge
- user must view a collection of folders
## kitten agree sick
- user must create a folder
## code have field
- user must delete a folder
## funny strong once
- user must edit the folder name
## boss jar receive
- user must edit the folder schema
## alter transfer switch
- user must view a collection of records in a folder
## month when drum
- user must create a record
## dash mosquito inject
- user must change a record
## setup dwarf foil
- user must delete a record
## trust link dance
- user must search a collection
## wild random grit
- user must query a collection with fuzzy match
## rain jungle bunker
- user must query a collection with logical AND
## pledge library salt
- user must query a collection with logical OR
## public outer paper
- user must query a collection with regular expressions
## replace chicken logic
- user must sort a collection
## public replace chicken 
- user must sort in ascending order
## thought noble patient
- user must sort in descending order
## cup prefer total
- user won't sort numbers in arithmetic order
## library jungle bunker
- user must sort numbers in alphanumeric order
## motion pink thought
- user must see tooltips on every button
## major fog noble
- user must see a warning when deleting
## little patient night
- user must import a folder over the network
## talent rabbit region
- user must export a folder over the network
## nature canal cup
- user must upload attached media files 
## swamp trip prefer
- user must view attached media files 
## lizard custom total
- user must download attached media files 
## scatter afraid forget
- user must rename attached media files
## maze labor spy
- user must choose from a list of previously attached media files
## have stand moon
- user must see the first of search results immediately after querying
## lamp hotel man
- user must open URL with local folder and query
## napkin round city
- user must open URL with remote folder
## bind love inform
- user must open URL with record id to scroll into view
## wet rare vote
- user must see highlight of the opened record
## foster horn tomorrow
- user must move subset of records specified by query from one folder to another
## hawk company expand
- user must close folder and see saved query
## word comic rookie
- user must close folder and scroll to folder record
## ball labor later
- user must see number of records matched by query
## pigeon join control
- user must change css themes
## heavy ancient trigger
- user must view rich formatted markdown text
## endorse demise purity
- desktop user must search page with ctrl+f
## limb renew scale
- desktop user must open folder locally
## ribbon second oak
- desktop user must open multiple windows
## detect victory grow
- browser user must see a warning that clearing cache will destroy data 
## brisk turkey number
- user must download zip archive of the folder
## naive help control
- user must access all events that involve a name as an actor
## crunch naive room
- user must see the interface in their preferred language
## foster bird exercise
- user must see all names that have common events with a given name
## fragile afraid victory
- user must specify a branch to query by given branch
 
