# Cloning from git

To clone a dataset from a remote server, press "+" at the root overview screen to create a new dataset, locate the dialogue "Add a remote?" and press "yes". Input the remote address after "conects to" and an access token after "token" if needed. After you press "save", the application will attempt to clone the repo and add it to the root overview list.

To learn more about evenor, see other [User Guides](./user_guides.md).
