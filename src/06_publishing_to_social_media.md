# Publishing to social media

> Another example is the management of creative works. Many of us have the opportunity to produce daily works that were previously given the status of art. Messages in messengers - epistolary art. Posts in social networks - essays and autobiography. Music and drawings began to be produced so simply that some do not know what to do with a flurry of works. We push all these works into services that are poorly designed for them, hard to read and live only for a few years - in social networks, in phone notes, in Google drives, in Word files. All the while, these works are significant events with metadata that embeds them in the overall path of our lives.

Evenor is designed to work with events alongside another syndication tool. Wrote the text and published it in three social networks. Wrote a song and published to three music platforms. Recorded a speech, sent it to a friend in messages, deciphered it with a neural network and uploaded it to the blog.

To learn more about evenor, see other [User Guides](./user_guides.md).
