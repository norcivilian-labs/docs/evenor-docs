# Caching a hard drive

> For example, file management. Folders with names like "2023-Sochi" lose their availability already in the third year of a person's life, when there are thirty-six of them. The myriad files that some of us hoard on hard drives are organized in a file system where they shouldn't be organized. -- From *Dictatum of Soul*

First, run the csvs-cli to collect metadata for all files in a given archive directory. Then, import the resulting csvs dataset in Evenor. Now you can find "All university text files from May 2006" and "all photos from each of my birthdays".

To learn more about evenor, see other [User Guides](./user_guides.md).
