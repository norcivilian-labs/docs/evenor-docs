# Navigation links

Sometimes you see a value and want to search all records that have it. To skip manually pasting the value in the search bar, long tap on the value anywhere in the app and choose the `Search` option from the list.

You can define your own navigation links in the folder profile. To do this, select a folder, press `edit`, locate the dialogue `Add a nagivation link?` and press yes. Input the branch that you want to search for, and the branch of a given value and press `Save`.

> On desktop, right click instead of the long tap to open the context menu.

To learn more about evenor, see other [User Guides](./user_guides.md).
