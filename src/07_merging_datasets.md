# Merging datasets

Locate the "Add a sync" dialogue in a dataset profile and press "yes". Input the name of another dataset and press "save". On the root profile view screen locate the dialogue "Sync with dataset?" and press "yes". Data from another dataset will be merged with the current one.

To learn more about evenor, see other [User Guides](./user_guides.md).
