# Running search queries

The search bar finds matches on the screen by default. When entered keywords, search bar filters the list for records that match a branch with the keyword value.

To learn more about evenor, see other [User Guides](./user_guides.md).
