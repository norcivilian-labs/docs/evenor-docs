# Editing dataset schema

Schema describes the structure, the entities inside a dataset. To edit the schema, select the dataset and press the "edit" button. Locate a dialogue "Add a branch?" and press "yes". 

> INFO: Branch is evenor's name for any entitity inside a dataset. Imagine that a dataset is a grove of trees, and each tree is made up of branches connected to each other. Branch is called a trunk if it has leaves - attributes that describe it. A branch without leaves is called a twig and sits at the very top of the tree. A branch that does not describe any other branch and does not have a trunk is called a root and sits at the very bottom of a tree. 

To learn more about evenor, see other [User Guides](./user_guides.md).
