# evenor-docs

Documentation for [evenor](https://gitlab.com/norcivilian-labs/evenor), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
